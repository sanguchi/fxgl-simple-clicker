package demo;

import com.almasb.fxgl.app.GameApplication;
import com.almasb.fxgl.app.GameSettings;
import com.almasb.fxgl.entity.Entity;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;

import java.util.Map;

import static com.almasb.fxgl.dsl.FXGL.*;

public class BasicGameApp extends GameApplication {

    public static int CIRCLE_SIZE = 20;
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    protected void initSettings(GameSettings gameSettings) {
        gameSettings.setWidth(400);
        gameSettings.setHeight(400);
        gameSettings.setTitle("Basic Clicker App");
    }

    @Override
    protected void initGame() {
        spawnEntity();
    }

    @Override
    protected void initGameVars(Map<String, Object> vars) {
        vars.put("score", 0);
        super.initGameVars(vars);
    }

    @Override
    protected void initUI() {
        Text score = new Text(30, 30, "0");
        Text close = new Text(getAppWidth() - 50, 10, "Close [x]");
        close.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {System.exit(0);});
        score.textProperty().bind(getip("score").asString());
        addUINode(score);
        addUINode(close);

        super.initUI();
    }

    private void spawnEntity() {
        Entity entity;
        entity = entityBuilder()
                .view(new Circle(CIRCLE_SIZE, Color.BLACK))
                .at(Math.random() * (getAppWidth() - CIRCLE_SIZE), Math.random() * (getAppHeight() - CIRCLE_SIZE))
                .buildAndAttach();

        entity.getViewComponent().addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            entity.removeFromWorld();
            spawnEntity();
            inc("score", +1);
        });
    }
}
